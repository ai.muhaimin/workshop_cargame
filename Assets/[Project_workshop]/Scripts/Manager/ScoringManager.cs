using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ScoringManager : MonoBehaviour
{
    public AudioSource soundCoin;
    public int ScorePointPlus = 1;
    public int ScorePoint;
    public TMP_Text ScoreText;

    private void Start()
    {
        ScorePoint = 5;
        ScoreText.text = ScorePoint.ToString();
    }

    public void AddScore()
    {
        ScorePoint = ScorePoint + ScorePointPlus;
        ScoreText.text = ScorePoint.ToString();
        soundCoin.Play();
    }

    public void ReduceScore()
    {
        ScorePoint = ScorePoint - ScorePointPlus;
        if (ScorePoint < 0)
        {
            ScorePoint = 0;
        }
        ScoreText.text = ScorePoint.ToString();
        soundCoin.Play();
    }
}
