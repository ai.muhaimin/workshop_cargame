using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
   public void ChangeSceneIndex(int indexScene)
    {
        SceneManager.LoadScene(indexScene);
    }

    public void ChangeScenename(string scenename)
    {
        SceneManager.LoadScene(scenename);
    }
}
