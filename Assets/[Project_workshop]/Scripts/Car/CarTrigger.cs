using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarTrigger : MonoBehaviour
{
    public ScoringManager ScoringManager;
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("kena:" + collision.gameObject.name);

        if(collision.gameObject.name == "coin")
        {
            collision.gameObject.GetComponent<Collider>().enabled = false;
            collision.gameObject.GetComponent<MeshRenderer>().enabled = false;
            ScoringManager.AddScore();
            
        }

        if (collision.gameObject.name == "box")
        {
            collision.gameObject.GetComponent<Collider>().enabled = false;
            collision.gameObject.GetComponent<MeshRenderer>().enabled = false;
            ScoringManager.ReduceScore();

        }
    }
}
