using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    //ini variable
    public bool isStartGame; // ini isinya true/false
    
    public Rigidbody rb;




    public float turnSpeed = 45.0f;
    public float speed = 20.0f;
    [SerializeField] float horizontalInput;
    [SerializeField] float forwardInput;

    // Start is called before the first frame update
    private void Awake()
    {
        Debug.Log("ini awake game");
    }
    void Start()
    {
        Debug.Log("ini start game");
        isStartGame = true;
        speed = 20;
        turnSpeed = 45;

        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isStartGame == true)
        {
            Debug.Log("ini looping");
            //RunCar();

            //if (Input.GetKey(KeyCode.Space))
            //{
            //    RunCar();
            //}

            horizontalInput = Input.GetAxis("Horizontal");
            forwardInput = Input.GetAxis("Vertical");

            transform.Translate(Vector3.forward * Time.deltaTime * speed * forwardInput);
            //transform.Translate(Vector3.right * Time.deltaTime * turnSpeed * horizontalInput);
            transform.Rotate(Vector3.up, turnSpeed * horizontalInput * Time.deltaTime);

        }

    }

    //public void RunCar()
    //{
    //    rb.MovePosition(transform.position + transform.forward * speed * Time.deltaTime);

    //}
}
